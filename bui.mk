# Clean a build tree before checking it in.
clobber:
	@rm -rf ltmain.sh autom4te.cache COPYING INSTALL aclocal.m4 \
		config.* configure depcomp install-sh missing *.out
	@find . -name "Makefile.in" | xargs rm -f
